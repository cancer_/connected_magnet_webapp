# WebApp de la connected_magnet

https://connectedmagnet.herokuapp.com/

# preparatif

* creer un compte heroku : https://signup.heroku.com/login
* creer une application depuis le dashboard en cliquant sur new : https://dashboard.heroku.com/apps
* creer un compte gitlab sur framagit : https://framagit.org/

# mettre en place votre webapp

* Forker le repo suivant depuis framagit : https://framagit.org/apprensemble/connected_magnet_webapp
* Aller dans settings -> CI/CD -> variables et passez à la configuration de la webapp

# configuration de la webapp

		Pour fonctionner la webapp doit se connecter à un serveur de message.
		Il existe des services gratuit sur le cloud pour les usages personnels.
		Pour ce tuto j'ai choisi mqtt cloud gratuit jusqu'à 5 connexions simultanées.

Comment se connecter?

Il vous faut definir dans gitlab-ci les variables d'environnements suivantes:

MQTT_CLOUD_URL mqtt://blabla.com:14441 #que vous trouverez dans les details mqtt cloud  
MQTT_USERNAME username  
MQTT_PASSWORD password  
TOPICS Max,Celeste,... # En gros une liste de prénom qui correspondra aux expediteurs  
HEROKU_APP_NAME mawebapp # Dans mon cas c'est connectedmagnet mais comme c'est pris il vous faudra trouver autre chose  
HEROKU_API_KEY bla-bla-bla # se trouve dans account settings / API Key de Heroku  

# Lancer le pipeline

* CD/CI -> pipeline -> Run pipeline
* Une fois le pipeline terminé le point suivant ce sera l'url de votre webapp

```shell
remote:        https://connectedmagnet.herokuapp.com/ deployed to Heroku     
```

# A faire

* reproduire chaque étape afin de completer la doc
